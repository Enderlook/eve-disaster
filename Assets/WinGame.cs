using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinGame : MonoBehaviour
{
    public string level;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 11)
        {
            SceneManager.LoadScene(level);
        }
    }
}
